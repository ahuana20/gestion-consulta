import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Router } from '@angular/router';
import { ArrendactoService } from '../services/arrendacto.service';
import { LocalStorageService } from '../services/local-storage.service';
import { UtilService } from '../services/util.service';
@Injectable({
  providedIn: 'root',
})
export class ValidarAfectacionRucGuard implements CanActivate {
  constructor(
    private localStorageService: LocalStorageService,
    private arrendactoService: ArrendactoService,
    private utilService: UtilService
  ) {}
  canActivate() {
    let numRuc = this.localStorageService.obtenerTipoDeUsuario().numRUC;
    return this.arrendactoService
      .validarAfectacionRUCPromise(numRuc)
      .then((res: any) => {
        if (!res.success) {
          this.utilService.modalMensajeError(res.msg);
          this.localStorageService.borrarSessionStorage();
        }
        return res.success;
      });
  }
}