import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SunatGuard implements CanActivate {
  canActivate(): Promise<boolean> | boolean {
    return new Promise((resolve, reject) => {
      if (!!!sessionStorage.getItem('SUNAT.token') &&
          !!!sessionStorage.getItem('SUNAT.currentData') &&
          !!!sessionStorage.getItem('SUNAT.userdata')) {
        window.location.assign('http://www.sunat.gob.pe');
        reject(false);
      } else {
        resolve(true);
      }
    });
  }
}
