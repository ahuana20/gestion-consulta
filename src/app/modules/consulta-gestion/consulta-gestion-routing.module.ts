import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards';
import { ConsultaContratosComponent } from './consulta-gestion.component';

const routes: Routes = [
  {
    path: '',
    component: ConsultaContratosComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class ConsultaContratosRoutingModule {}
