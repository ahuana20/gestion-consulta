import { Component, OnInit, ViewChild } from '@angular/core';
import * as config from 'src/app/utils/config';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ArrendaConsultaService } from 'src/app/services/arrendaconsulta.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Contrato } from 'src/app/models/contrato.model';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { UserData } from 'src/app/models/user-data.model';
import { ExportExcelService } from 'src/app/services/export-excel.services';
import { UtilService } from 'src/app/services/util.service';
import { ExportPdfService } from 'src/app/services/export-pdf.services';
// Excel
import { distinctUntilChanged } from 'rxjs/operators';
import { Constantes } from 'src/app/utils/constantes';
import { downloadPDF } from 'src/app/utils/utilitarios';
import * as moment from 'moment';
import { ExportModel } from 'src/app/models/export.model';
import {CustomDatepickerI18n} from '../../utils/ngdatepicker/custom-datepicker-i18n';
import {NgbDatepickerI18n} from '@ng-bootstrap/ng-bootstrap';
import {I18n} from '../../utils/ngdatepicker/i18n';
import {ArrendactoService} from '../../services/arrendacto.service';

@Component({
  selector: 'app-consulta-contratos',
  templateUrl: './consulta-gestion.component.html',
  styleUrls: ['./consulta-gestion.component.css'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
  ],
})
export class ConsultaContratosComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = config.dtOptions;
  dtTrigger: Subject<any> = new Subject();
  contribuyente: string = "";
  arrayContratos: Contrato[] = [];
  itemSeleccionado: string = null;
  form: FormGroup;
  lstTipDocIdentidad: any[] = [];
  numRucContribuyente: string;
  fecDeclaInformativaFinMin: any;
  fecDeclaInformativaFinMax: any;
  lstIndEstadoDecla: any[] = [];
  items: Contrato[] = [];

  simpleCharacteresAndNumber: string = 'integer'; // noSpecialChars || integer;
  numDocArrendatarioMaxlength: number = 8;
  constructor(
    private arrendaConsultaService: ArrendaConsultaService,
    private arrendactoService: ArrendactoService,
    private localStorageService: LocalStorageService,
    private exportExcelService: ExportExcelService,
    private utilService: UtilService,
    private exportPdfService: ExportPdfService
  ) { }

  ngOnInit(): void {
    this.initForm();
    const userData: UserData = this.localStorageService.obtenerTipoDeUsuario();
    /*this.numRucContribuyente = userData.numRUC;*/
    this.cargarCombosBandeja();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  cargarCombosBandeja() {
    this.lstTipDocIdentidad =
      this.localStorageService.cargarParametria('040').listParametros;

    this.lstIndEstadoDecla =
      this.localStorageService.cargarParametria('010').listParametros;
    this.lstIndEstadoDecla = this.lstIndEstadoDecla.filter((item: any) => {
      // tslint:disable-next-line:triple-equals
      return item.codParam != 0;
      });
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr(size * -1, size);
  }

  detallesToggled(item: any) {
    if (this.itemSeleccionado === item.id) {
      this.itemSeleccionado = null;
    } else {
      this.itemSeleccionado = item.id;
    }
  }

  initForm() {
    this.form = new FormGroup({
      numDocIde: new FormControl(null, [this.noWhitespaceValidator, Validators.required]),
      numDeclaInformativa: new FormControl(null, [this.noWhitespaceValidator, Validators.required]),
      codTipDocArrendatario: new FormControl('', []),
      numDocArrendatario: new FormControl(null, []),


      dependencia : new FormControl(null, []),
      codTipDocIdenArrendatario : new FormControl(null, []),
      numDocumentoSubarrendador : new FormControl(null, []),
      tipoActividad : new FormControl(),
      tipoBien : new FormControl(),
      codEstadoDeclaInformativa: new FormControl('', []),
      motivoContratoModificado : new FormControl('', []),
      fecDeclaInformativaIni: new FormControl(null, []),
      fecDeclaInformativaFin: new FormControl(null, []),

    });

    this.initSignals();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  initSignals() {
    //Validacion Ingreso nroRUC Propietario
    this.form.controls['numDocIde'].valueChanges.pipe(distinctUntilChanged()).subscribe(
      (newValue) => {
        if (newValue) {
          this.form.controls['numDeclaInformativa'].setValidators([]);
          this.form.controls['codTipDocArrendatario'].setValidators([]);
          this.form.controls['numDocArrendatario'].setValidators([]);
          this.form.controls['fecDeclaInformativaIni'].setValidators([]);
          this.form.controls['fecDeclaInformativaFin'].setValidators([]);
        }

        this.form.markAllAsTouched();
        this.form.controls['numDeclaInformativa'].updateValueAndValidity();
        this.form.controls['numDocArrendatario'].updateValueAndValidity();
        this.form.controls['codTipDocArrendatario'].updateValueAndValidity();
        this.form.controls['fecDeclaInformativaIni'].updateValueAndValidity();
        this.form.controls['fecDeclaInformativaFin'].updateValueAndValidity();
      }
    );

    //Validación del nro de declaración
    this.form.controls['numDeclaInformativa'].valueChanges.pipe(distinctUntilChanged()).subscribe(
      (newValue) => {
        if (newValue) {
          this.form.controls['codTipDocArrendatario'].setValidators([]);
          this.form.controls['numDocArrendatario'].setValidators([]);
          this.form.controls['fecDeclaInformativaIni'].setValidators([]);
          this.form.controls['fecDeclaInformativaFin'].setValidators([]);
          this.form.controls['codTipDocArrendatario'].disable();
          this.form.controls['numDocArrendatario'].disable();
          this.form.controls['fecDeclaInformativaIni'].disable();
          this.form.controls['fecDeclaInformativaFin'].disable();
        } else {
          this.form.enable();
        }
        this.form.markAllAsTouched();

        this.form.controls['numDocArrendatario'].updateValueAndValidity();
        this.form.controls['codTipDocArrendatario'].updateValueAndValidity();
        this.form.controls['fecDeclaInformativaIni'].updateValueAndValidity();
        this.form.controls['fecDeclaInformativaFin'].updateValueAndValidity();
      }
    );
    //Validación selección tipDoc Arrendatario.
    this.form.controls['codTipDocArrendatario'].valueChanges.pipe(distinctUntilChanged()).subscribe(
      (newValue) => {
        if (newValue) {
          this.form.controls['numDeclaInformativa'].setValidators([]);
          this.form.controls['numDocArrendatario'].setValidators([
            Validators.required,
          ]);
          this.form.controls['numDeclaInformativa'].disable();
          this.form.controls['fecDeclaInformativaIni'].disable();
          this.form.controls['fecDeclaInformativaFin'].disable();

          this.setCodTipDocArrendatarioValidators(newValue);
        } else {
          this.form.enable();
          this.form.controls['numDocArrendatario'].setValidators([]);
        }
        this.form.controls['numDocArrendatario'].setValue(null);
        this.form.markAllAsTouched();
        this.form.controls['numDeclaInformativa'].updateValueAndValidity();
        this.form.controls['numDocArrendatario'].updateValueAndValidity();
      }
    );

    this.form.controls['numDocArrendatario'].valueChanges.pipe(distinctUntilChanged()).subscribe(
      (newValue) => {
        if (newValue) {

          this.form.controls['numDeclaInformativa'].setValidators([]);
          this.form.controls['codTipDocArrendatario'].setValidators([
            Validators.required,
          ]);
        } else {
          this.form.controls['codTipDocArrendatario'].setValidators([]);
        }

        this.form.markAllAsTouched();

        this.form.controls['numDeclaInformativa'].updateValueAndValidity();
        this.form.controls['codTipDocArrendatario'].updateValueAndValidity();
      }
    );

    this.form.controls['fecDeclaInformativaIni'].valueChanges.pipe(distinctUntilChanged()).subscribe(
      (newValue) => {
        if (newValue) {
          this.fecDeclaInformativaFinMin = newValue;
          this.fecDeclaInformativaFinMax = {
            day: newValue.day,
            month: newValue.month,
            year: newValue.year + 1,
          };
          this.form.controls['numDeclaInformativa'].setValidators([]);

          this.form.controls['fecDeclaInformativaFin'].setValue(null);
          this.form.controls['fecDeclaInformativaFin'].setValidators([
            Validators.required,
          ]);

          this.form.controls['numDeclaInformativa'].disable();
          this.form.controls['codTipDocArrendatario'].disable();
          this.form.controls['numDocArrendatario'].disable();

        } else {
          this.form.controls['fecDeclaInformativaFin'].setValidators([]);
          this.form.enable();

        }

        this.form.markAllAsTouched();
        this.form.controls['numDeclaInformativa'].updateValueAndValidity();
        this.form.controls['fecDeclaInformativaFin'].updateValueAndValidity();
      }
    );

    this.form.controls['fecDeclaInformativaFin'].valueChanges.pipe(distinctUntilChanged()).subscribe(
      (newValue) => {
        if (newValue) {
          this.form.controls['numDeclaInformativa'].setValidators([]);
          this.form.controls['fecDeclaInformativaIni'].setValidators([
            Validators.required,
          ]);
        } else {
          this.form.controls['fecDeclaInformativaIni'].setValidators([]);
        }

        this.form.markAllAsTouched();
        this.form.controls['numDeclaInformativa'].updateValueAndValidity();
        this.form.controls['fecDeclaInformativaIni'].updateValueAndValidity();
      }
    );

    this.form.controls['codEstadoDeclaInformativa'].valueChanges.pipe(distinctUntilChanged()).subscribe(
      (newValue) => {
        if (newValue) {
          this.form.controls['numDeclaInformativa'].setValidators([]);
        } else {
          this.form.enable();
        }

        this.form.controls['numDeclaInformativa'].updateValueAndValidity();
      });
  }

  buscar() {
    const request: any = this.form.getRawValue();

    request.fecDeclaInformativaIni = this.dateToFormat(
      request.fecDeclaInformativaIni
    );
    request.fecDeclaInformativaFin = this.dateToFormat(
      request.fecDeclaInformativaFin
    );
    this.numRucContribuyente = request.numDocIde;
    this.arrendaConsultaService
      .contratoConsulta(this.numRucContribuyente, request)
      .toPromise()
      .then((items: Contrato[]) => {
        this.arrayContratos = [];
        this.arrayContratos = items;
        // @ts-ignore
        this.arrayContratos.forEach((item: any) => {
          item['arrendatario.desTipDocIde'] = item.arrendatario.desTipDocIde;
          item['arrendatario.numDocIde'] = item.arrendatario.numDocIde;
          item['arrendatario.nomCompleto'] = item.arrendatario.nomCompleto;
          item.fecDeclaInformativa = this.formatDate(item.fecDeclaInformativa, '');
          });
        this.rerender();
      })
      .catch((error: any) => {
        this.utilService.modalConfirmacion(
          Constantes.MODAL_TITULO,
          Constantes.MENSAJES_NO_DATA,
          Constantes.MODAL_ENTENDIDO,
          Constantes.MODAL_PRIMARY,
          () => {},
          () => {}
        );
        this.arrayContratos = [];
        this.rerender();
      });
  }

  dateToFormat(date: any) {
    if (date === '' || date === undefined || date === null) {
      return '';
    }

    return `${date.day}-${date.month}-${date.year}`;
  }

  // tslint:disable-next-line:typedef
  formatDate(date: string, format: string) {
    if (!date) {
      return '--';
    }
    return moment(date, format).format('DD[/]MM[/]YYYY');
  }

  limpiar() {
    this.form.controls['numDocIde'].setValue(null, { emitEvent: false });
    this.form.controls['numDeclaInformativa'].setValue(null, { emitEvent: false });
    this.form.controls['codTipDocArrendatario'].setValue('', { emitEvent: false });
    this.form.controls['numDocArrendatario'].setValue(null, { emitEvent: false });
    this.form.controls['fecDeclaInformativaIni'].setValue(null, { emitEvent: false });
    this.form.controls['fecDeclaInformativaFin'].setValue(null, { emitEvent: false });
    this.form.controls['codEstadoDeclaInformativa'].setValue('', {emitEvent: false});

    this.form.controls['numDocIde'].setValidators([Validators.required]);
    this.form.controls['numDeclaInformativa'].setValidators([Validators.required]);
    this.form.controls['codTipDocArrendatario'].setValidators([]);
    this.form.controls['numDocArrendatario'].setValidators([]);
    this.form.controls['fecDeclaInformativaIni'].setValidators([]);
    this.form.controls['fecDeclaInformativaFin'].setValidators([]);

    this.form.controls['numDocIde'].updateValueAndValidity();
    this.form.controls['numDeclaInformativa'].updateValueAndValidity();
    this.form.controls['codTipDocArrendatario'].updateValueAndValidity();
    this.form.controls['numDocArrendatario'].updateValueAndValidity();
    this.form.controls['fecDeclaInformativaIni'].updateValueAndValidity();
    this.form.controls['fecDeclaInformativaFin'].updateValueAndValidity();

    this.fecDeclaInformativaFinMin = null;
    this.fecDeclaInformativaFinMax = null;
    this.contribuyente = "";

    this.arrayContratos = [];
    this.rerender();
  }

  disabledBuscar(): boolean {
    return (
      this.form.controls['numDocIde'].hasError('required') ||
      this.form.controls['numDeclaInformativa'].hasError('required') ||
      this.form.controls['codTipDocArrendatario'].hasError('required') ||
      this.form.controls['numDocArrendatario'].hasError('required') ||
      this.form.controls['fecDeclaInformativaIni'].hasError('required') ||
      this.form.controls['fecDeclaInformativaFin'].hasError('required')
    );
  }

  exportExcel() {
    const model: ExportModel = new ExportModel();
    model.title = 'Resultados de contratos';
    model.data = this.arrayContratos;
    model.headers = [
      'Número Declaración Informativa',
      'Fecha de generación',
      'Tipo de documento',
      'Doc. Iden. Inquilino',
      'Nombres y Apellidos/ Razón Social',
      'Estado declarac.',
    ];
    model.keys = [
      'numDeclaInformativa',
      'fecDeclaInformativa',
      'arrendatario.desTipDocIde',
      'arrendatario.numDocIde',
      'arrendatario.nomCompleto',
      'desEstadoDeclaInformativa',
    ];

    this.exportExcelService.exportExcel(model);
  }

  exportPDF() {
    const model: ExportModel = new ExportModel();
    model.title = 'Resultados de contratos';
    model.data = this.arrayContratos;
    model.headers = [
      'Número Declaración Informativa',
      'Fecha de generación',
      'Tipo de documento',
      'Doc. Iden. Inquilino',
      'Nombres y Apellidos/ Razón Social',
      'Estado declarac.'
    ];
    model.keys = [
      'numDeclaInformativa',
      'fecDeclaInformativa',
      'arrendatario.desTipDocIde',
      'arrendatario.numDocIde',
      'arrendatario.nomCompleto',
      'desEstadoDeclaInformativa'
    ];

    this.exportPdfService.exportPDF(model, true);
  }

  descargarPDF(e: any, numDecla: any) {
    e.target.disabled = true;
    this.arrendaConsultaService.obtenerReporteDeclaracion(numDecla.toString()).subscribe(response => {
      if (response.success) {
        downloadPDF(response.data, response.msg);
        e.target.disabled = false;
      } else {
        this.abrirMensajeError(Constantes.ERROR_DESCARGA_CONTRATO);
      }
    });
  }

  abrirMensajeError(msg: String) {
    this.utilService.modalMensajeError(msg);
  }

  setCodTipDocArrendatarioValidators(newValue: string) {
    switch (newValue) {
      case Constantes.COD_TIP_DOCIDE_DNI:
        this.simpleCharacteresAndNumber = 'integer';
        this.numDocArrendatarioMaxlength = 8;
        this.form.controls['numDocArrendatario'].setValidators([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(8),
        ]);
        break;
      case Constantes.COD_TIP_DOCIDE_CE:
        this.simpleCharacteresAndNumber = 'noSpecialChars';
        this.numDocArrendatarioMaxlength = 12;
        break;
      case Constantes.COD_TIP_DOCIDE_RUC:
        this.simpleCharacteresAndNumber = 'integer';
        this.numDocArrendatarioMaxlength = 11;
        this.form.controls['numDocArrendatario'].setValidators([
          Validators.required,
          Validators.minLength(11),
          Validators.maxLength(11),
        ]);
        break;
      case Constantes.COD_TIP_DOCIDE_PASAPORTE:
        this.simpleCharacteresAndNumber = 'noSpecialChars';
        this.numDocArrendatarioMaxlength = 12;
        break;
      case Constantes.COD_TIP_DOCIDE_P_NAC:
        this.simpleCharacteresAndNumber = 'noSpecialChars';
        this.numDocArrendatarioMaxlength = 15;
        break;
      default:
        this.simpleCharacteresAndNumber = 'noSpecialChars';
        this.numDocArrendatarioMaxlength = 15;
        break;
    }
  }
}
