import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaContratosRoutingModule } from './consulta-gestion-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { ConsultaContratosComponent } from './consulta-gestion.component';
import { SoloNumerosDirective } from '../../utils/directives/solo-numeros.directive';
import { SimpleCharacteresAndNumberDirective } from 'src/app/utils/directives/simple-characteres-and-number.directive';

@NgModule({
  declarations: [
    ConsultaContratosComponent,
    SoloNumerosDirective,
    SimpleCharacteresAndNumberDirective
  ],
  imports: [
    CommonModule,
    ConsultaContratosRoutingModule,
    NgbModule,
    FormsModule,
    DataTablesModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot({
      showMaskTyped: false,
      dropSpecialCharacters: true,
      // clearIfNotMatch : true
    }),
  ]
})
export class ConsultaContratosModule {}
