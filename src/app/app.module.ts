import { BrowserModule } from '@angular/platform-browser';
import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  APP_INITIALIZER,
} from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MensajeAutorizacionComponent } from './components/utils/mensaje-autorizacion/mensaje-autorizacion.component';
import { ModalMensajeComponent } from './components/utils/modal-mensaje/modal-mensaje.component';
import { ThemeSpinnerService } from './theme/services/theme-spinner.service';
import { LocalStorageService } from './services/local-storage.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderInterceptor } from './interceptors/httpconfig.interceptor';
import { NgbDateParsearFormato } from './utils/ngb-date-parsear-formato';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { SoloNumerosDirective } from './utils/directives/solo-numeros.directive';
import { UppercaseFormsDirective } from './utils/directives/uppercase-forms.directive';
import { ModalConfirmacionComponent } from './components/utils/modal-confirmacion/modal-confirmacion.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MyHttpInterceptor } from './interceptors/request.interceptor';
import { ConsultaContratosModule } from './modules/consulta-gestion/consulta-gestion.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export function guardarParametria(config: LocalStorageService) {
  return (): Promise<any> => {
    return config.guardarParametria();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    MensajeAutorizacionComponent,
    ModalMensajeComponent,
    ModalConfirmacionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ConsultaContratosModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
  ],
  exports: [],
  providers: [
    ThemeSpinnerService,
    NgbActiveModal,
    { provide: NgbDateParserFormatter, useClass: NgbDateParsearFormato },
    { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
    LocalStorageService,
    {
      provide: APP_INITIALIZER,
      useFactory: guardarParametria,
      multi: true,
      deps: [LocalStorageService],
    },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent],
})
export class AppModule {}
