import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ValidarAfectacionRucGuard } from './guards/validar-afectacion-ruc.guard';
import { SunatGuard } from './guards';
const routes: Routes = [
  {
    path: 'contratos',
    canActivate: [SunatGuard],
    loadChildren: () =>
      import('./modules/consulta-gestion/consulta-gestion.module').then(
        (m) => m.ConsultaContratosModule
      ),
  },
  {
    path: '',
    redirectTo: 'contratos',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'contratos',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
