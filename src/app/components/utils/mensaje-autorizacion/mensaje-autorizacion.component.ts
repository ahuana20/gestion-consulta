import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mensaje-autorizacion',
  templateUrl: './mensaje-autorizacion.component.html',
  styleUrls: ['./mensaje-autorizacion.component.css']
})
export class MensajeAutorizacionComponent implements OnInit {

  constructor(
    public modalService: NgbModal,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
  }
  
  aceptar(): void {
    location.href = 'https://www1.sunat.gob.pe/loader/recaudaciontributaria/administracion/internet/contrato-arrendamiento';
  }

}
