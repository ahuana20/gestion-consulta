import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConstantesCadenas } from 'src/app/utils/constantes-cadenas';
@Component({
  selector: 'app-modal-mensaje',
  templateUrl: './modal-mensaje.component.html',
  styleUrls: ['./modal-mensaje.component.css']
})
export class ModalMensajeComponent implements OnInit {

  constructor(public modalService: NgbActiveModal) { }
  resultado: number;
  @Input() public modal;
  @Input() public nameTab = '';
  @Output() respuesta = new EventEmitter<any>();

  ngOnInit(): void {
  }
  aceptar() {
   
    if (this.nameTab === '') {
      this.modalService.close();
    } else if (this.nameTab === ConstantesCadenas.TAB_DECLARACION_RECT_SUST) {
    
      this.resultado = 1;
      this.modalService.close(this.resultado);
    } else {
     
      this.respuesta.emit(ConstantesCadenas.RESPUESTA_SI);
      this.modalService.close();
    }
  }

  cancelar() {
  
    if (this.nameTab === ConstantesCadenas.TAB_DECLARACION_RECT_SUST) {
      this.resultado = 0;
      this.modalService.close(this.resultado);
    } else {
      
      this.respuesta.emit(ConstantesCadenas.RESPUESTA_NO);
      this.modalService.close();
    }
  }
}
