import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ThemeSpinnerService } from '../theme/services/theme-spinner.service';
import { MensajeAutorizacionComponent } from '../components/utils/mensaje-autorizacion/mensaje-autorizacion.component';
import { ModalMensajeComponent } from '../components/utils/modal-mensaje/modal-mensaje.component';
import { HttpError } from '../models/http-error';
import { catchError } from 'rxjs/operators';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  modalOption: NgbModalOptions = {};
  titulo = 'Error';
  mensaje = 'Error desconocido';

  constructor(
    private modalService: NgbModal,
    private _spinner: ThemeSpinnerService
  ) {}

  private getticket(): string {
    const user = JSON.parse(sessionStorage.getItem('SUNAT.userdata'));
    if (user) {
      return user.login + '-' + user.ticket;
    } else {
      return null;
    }
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const logFormat = `background: red; color: white`;
    const token: string = sessionStorage.getItem('SUNAT.token');
    const ticket = this.getticket();
    let solicitud = request;

    if (token) {
      solicitud = request.clone({
        setHeaders: {
          authorization: `Bearer ${token}`,
          'x-custom-ticket': ticket,
        },
      });
    } else {
      this._spinner.hide();
    }
    return next.handle(solicitud).pipe(
      catchError((error: any) => {
        if (error instanceof HttpErrorResponse) {
          switch (error.status) {
            case HttpError.BadRequest:
              this.mensaje = 'Error 400 en el servidor';
              console.error('%c error 400', logFormat);
              break;
            case HttpError.Unauthorized:
              this.mostrarMensaje401();
              return throwError(error);
            case HttpError.NotFound:
              this.titulo = 'HTTP 404';
              this.mensaje = 'El recurso solicitado no existe';
              break;
            case HttpError.TimeOut:
              this.mensaje = 'Error 408 en el Servidor';
              break;
            case HttpError.InternalServerError:
              this.titulo = 'Error del Servidor';
              this.mensaje =
                'Señor contribuyente disculpe la molestia, en estos momentos no se ' +
                'puede acceder a los servicios de SUNAT, por favor reintentar en 5 minutos';

              break;
            case HttpError.GatewayTimeout:
              this.titulo = 'Tiempo de espera';
              this.mensaje = `no puede completar tu solicitud dentro del marco de tiempo dado
                      (Status ${HttpError.GatewayTimeout})`;
              break;
            default:
              this.mensaje =
                'Señor contribuyente disculpe la molestia,' +
                ' en estos momentos no se puede acceder a los servicios de SUNAT, por favor reintentar en 5 minutos.';
          }
        }

        if (error.status !== 422) {
          const message = {
            titulo: this.titulo,
            mensaje: this.mensaje,
          };
          const modalRef = this.modalService.open(
            ModalMensajeComponent,
            this.modalOption
          );
          modalRef.componentInstance.modal = message;
          modalRef.componentInstance.nameTab = '';
          this._spinner.hide();
        }

        return throwError(error);
      })
    );
  }

  mostrarMensaje401(): void {
    this.modalService.open(MensajeAutorizacionComponent, {
      backdrop: 'static',
      keyboard: false,
    });
  }
}
