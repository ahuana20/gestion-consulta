export class SubArrendatario {
    codTipDocIde: string;
    desTipDocIde: string;
    numDocIde: string;
    nomCompleto: string;
    apePaterno: string;
    apeMaterno: string;
    nomSubArrendatario: string;
    numCelular: string;
    dirCorreo1: string;
    dirCorreo2: string;
}
