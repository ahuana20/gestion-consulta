import { Contrato } from "./contrato.model"

export class ResponseRequest {
    code: string;
    msg: string;
    data: Contrato [];
    success: boolean
}