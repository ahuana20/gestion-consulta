export class CuponeraDetalle {
    numOrden?: number;
    codNPS?: string;
    perDeclaracion?: string;
    fecInicioPeriodo?: string;
    fecFinPeriodo: string;
    codTipoMoneda: string;
    desTipoMoneda?: string;
    mtoAlquilerPeriodo: number;
    mtoImpuestoPeriodo: number;
    valTasaRenta?: number;
    fecVencimientoNPS: string;
    indEstadoVigencia: string;
    desEstadoVigencia?: string;
    indEstadoPagoNPS?: string;
    desEstadoPagoNPS?: string;
    indFormaPagoNPS?: string;
    codOperacionPagoNPS?: string;
    fecPagoNPS?: string;
}
