export class UserData {
    numRUC?: string;
    codDepend?: string;
    apeMaterno?: string;
    apePaterno?: string;
    nombres?: string;
    nombreCompleto?: string;
    ticket?: string;
    login: string;
    codTipdoc: string;
    nroRegistro: string
    constructor() {
        this.numRUC = '10082420822';
        this.codDepend = '0023';
        this.apeMaterno ='';
        this.apePaterno ='';
        this.nombres = 'MI EMPRESA DE DESARROLLO';
        this.nombreCompleto = 'MI EMPRESA DE DESARROLLO';
        this.ticket = '1010';
        this.login = '0101';
        this.codTipdoc = '0001';
        this.nroRegistro = '6758';
    }
}
