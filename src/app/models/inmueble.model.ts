export class Inmueble {
    codTipoPredio: string;
    desTipoPredio: string;
    numPartidaRegistral: string;
    codDepartamento: string;
    desDepartamento: string;
    codProvincia: string;
    desProvincia: string;
    codDistrito: string;
    desDistrito: string;
    codTipoVia: string;
    desTipoVia: string;
    nomVia: string;
    codTipoZona: string;
    desTipoZona: string;
    nomZona: string;
    numPropiedad: string;
    numManzana: string;
    numInteriorDpto: string;
    numKilometro: string;
    numLote: string;
    valAutovaluo: number;
}
