export class ExportModel {
  title: string;
  data: any[];
  headers: any[];
  keys: any[];
}
