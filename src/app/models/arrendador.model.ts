export class Arrendador {
    codTipDocIde: string;
    desTipDocIde: string;
    numDocIde: string;
    nomCompleto: string;
    apePaterno: string;
    apeMaterno: string;
    nomArrendador: string;
}
