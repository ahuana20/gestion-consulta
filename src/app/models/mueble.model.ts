export class Mueble {
    codTipoMueble: string;
    desTipoMueble: string;
    numPartidaRegistral: string;
    nomModelo: string;
    nomMarca: string;
    numSerie: string;
    numPlacaRodaje: string;
    numCilindrada: string;
    annFabrica: number;
    desBienMueble: string;
}
