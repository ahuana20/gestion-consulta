import { Arrendador } from './arrendador.model';
import { Arrendatario } from './arrendatario.model';
import { CuponeraDetalle } from './cuponera-detalle.model';
import { SubArrendatario } from './sub-arrendatario.model';

export class Cuponera {
    numDeclaInformativa: number;
    numCuponera: number;
    fecIniContrato: string;
    fecFinContrato: string;
    fecBaja?: Date;
    arrendador?: Arrendador;
    arrendatario?: Arrendatario;
    subArrendatario?: SubArrendatario;
    codMotivoBaja?: string;
    desMotivoBaja?: string;
    codEstadoCuponera: string;
    desEstadoCuponera: string;
    listCuponera?: CuponeraDetalle[];
}
