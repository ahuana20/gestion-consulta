export class Arrendatario {
    codTipDocIde: string;
    desTipDocIde: string;
    numDocIde: string;
    nomArrendatario: string;
    apePaterno: string;
    apeMaterno: string;
    nomCompleto: string;
    numCelular: string;
    dirCorreo1: string;
    dirCorreo2: string;
}
