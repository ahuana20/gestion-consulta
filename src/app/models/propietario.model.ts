export class Propietario {
    codTipDocIde: string;
    desTipDocIde: string;
    numDocIde: string;
    apePaterno?: string;
    apeMaterno?: string;
    nomPropietario: string;
    nomCompleto: string;
    porParticipacion: number;
    indEsArrendador: string;
}
