import { Inmueble } from "./inmueble.model";
import { Mueble } from "./mueble.model";
export class BienAlquilado {
    codTipoBien: string;
    desTipoBien: string;
    inmueble?: Inmueble;
    mueble?: Mueble;
}
