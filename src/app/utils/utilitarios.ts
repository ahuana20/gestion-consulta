import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Constantes } from './constantes';

export function trim(value: any): string {
    if (isEmpty(value)) {
        return '';
    }
    return value.toString().trim();
}

export function isEmpty(value: any): boolean {
    if (value == null || value == undefined) {
        return true;
    }
    if (value.__proto__.constructor === String) {
        return value.trim().length === 0;
    }
    if (value.__proto__.constructor === Array) {
        return value.length === 0;
    }
    if (value.__proto__.constructor === Object) {
        return Object.getOwnPropertyNames(value).length === 0;
    }
    return false;
}

export function isNotEmpty(value: any): boolean {
    return !isEmpty(value);
}

export function toNumber(value: any): number {
    if (isEmpty(value)) {
        return null;
    }
    return parseInt(value);
}

export function isNumber(value: any): value is number {
    return !isNaN(toNumber(value));
}


export function padNumber(value: number) {
    if (isNumber(value)) {
        return `0${value}`.slice(-2);
    } else {
        return '';
    }
}

export function stringToNgbDateStruct(value: string) : NgbDateStruct {
    if (isEmpty(value)) {
        return  null;
    }
    const date = value.split(Constantes.DELIMITER_DATE);
    return {
      day: parseInt(date[0], 10),
      month: parseInt(date[1], 10),
      year: parseInt(date[2], 10)
    };
}
export function ngbDateStructToString(date: NgbDateStruct) : string {
    if (isEmpty(date)) {
        return  '';
    }
    return `${padNumber(date.day)}${Constantes.DELIMITER_DATE}${padNumber(date.month)}${Constantes.DELIMITER_DATE}${date.year}`;
}
export function stringToDate(date: string, format: string): Date {
    return moment(date, format).toDate();
}
export function dateToString(date: Date, format: string): string {
    return moment(date).format(format);
}

function base64ToPDF(cadenaB64: any): any {
    let data = cadenaB64;
    let mimeType = 'application/pdf';
    let byteCharacters = atob(data);
    let byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    let byteArray = new Uint8Array(byteNumbers);
    let file = new Blob([byteArray], { type: mimeType + ';base64' });
    let fileURL = URL.createObjectURL(file);

    return fileURL;
}

export function downloadPDF(cadenaB64: any, nomArchivo: string) {
    let fileURL = base64ToPDF(cadenaB64);
    const downloadLink = document.createElement("a");
    const fileName = nomArchivo;
    downloadLink.target = '_blank'
    downloadLink.href = fileURL;
    downloadLink.download = fileName;
    downloadLink.click();
    downloadLink.remove();
}

export function printPDF(cadenaB64: any) {
    let fileURL = base64ToPDF(cadenaB64);
    window.open(fileURL).print();
}

