import { environment } from 'src/environments/environment';
export class ConstantesUris {
    static URI_PARAMETRIA = environment.uri_base + 'parametros';
    static URI_PERMITIR_DECLARACION = environment.uri_base + 'permitirDeclaracion/{numRuc}';
    static URI_CONTRATO_CONSULTA = environment.url_consulta + 'contrato/consulta/{numRuc}?{params}';
    static URI_REPORTE_CONTRATO = environment.uri_base + "reporteContrato/{numDeclaInfor}";
}
