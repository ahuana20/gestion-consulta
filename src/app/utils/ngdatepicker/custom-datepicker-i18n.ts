import {Component, Injectable} from '@angular/core';
import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { I18n } from './i18n';
import * as config from '../config';


const I18N_VALUES = {
  'es': {
    weekdays: ['L', 'M', 'M', 'J', 'V', 'S', 'D'],
    months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
  }
 
};

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  

  constructor(private _i18n: I18n) {
    super();
  }
  
  getWeekdayShortName(weekday: number): string {
    return config.I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return config.I18N_VALUES[this._i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}