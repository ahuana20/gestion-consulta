export class Constantes {
    static readonly MODAL_OK = 1;
    static readonly MODAL_ACEPTAR_SALIR = 2;
    static readonly MODAL_ACEPTAR_CANCELAR = 3;
    static readonly MODAL_CANCELAR_CONFIRMAR = 4;
    static readonly MODAL_NO_SI = 5;
    static readonly MODAL_ENTENDIDO = 6;
    static readonly MODAL_PRIMARY = 'primary';
    static readonly MODAL_DANGER = 'danger';
    static readonly MODAL_SUCCESS = 'success';
    static readonly MODAL_DEFAULT = 'default';
    static readonly MODAL_WARNING = 'warning'
    static readonly MODAL_TITULO = 'Mensaje';
    static readonly MODAL_ERROR_TITULO = 'Error';
    static readonly MODAL_INFO_TITULO = 'Mensaje';
    static readonly MODAL_SUCCESS_TITULO = 'Operaci\u00f3n exitosa';
    static readonly DELIMITER_DATE = '-';
    static readonly CONTRATO_PENDIENTE = 'Usted tiene una declaraci\u00f3n informativa pendiente N°: {numDeclaInformativa}. ¿Desea continuar? En caso de no continuar se eliminar\u00e1 la anterior declaraci\u00f3n.'
    static readonly ELIMINACION_CORRECTA_CONTRATO_PENDIENTE = 'Se elimin\u00f3 correctamente el Contrato Pendiente';
    static readonly ERROR_ELIMINACION_CONTRATO_PENDIENTE = 'Ocurri\u00f3 un problema al momento de eliminar el Contrato Pendiente.';

    static readonly DARBAJA = '¿Esta seguro de DAR DE BAJA al registro de la declaración informativa?';
    static readonly MODAL_BAJA_TITULO = 'Mensaje';

    static readonly CODIGO_TIPO_BIEN_MUEBLE = '01';
    static readonly CODIGO_TIPO_BIEN_INMUEBLE = '02';
    static readonly ETIQUETA_CONTROL_BIEN_MUEBLE = 'Tipo de Bien Mueble';
    static readonly ETIQUETA_CONTROL_BIEN_INMUEBLE = 'Tipo de Predio';

    static readonly COD_TIPO_MUEBLE_VEHICULAR_AUTOMOTOR = '01';
    static readonly COD_TIPO_MUEBLE_NAVES_AERONAV_EMBAR = '02';
    static readonly COD_TIPO_MUEBLE_MAQUINARIA_EQUIPO = '03';

    static readonly ELIMAR_PROPIETARIO = '¿Est\u00e1 seguro de eliminar el Propietario?';
    static readonly COD_TIP_DOCIDE_DNI = '01';
    static readonly COD_TIP_DOCIDE_RUC = '06';
    static readonly COD_TIP_DOCIDE_CE = '04';
    static readonly COD_TIP_DOCIDE_PASAPORTE = '07';
    static readonly COD_TIP_DOCIDE_P_NAC = '11';
    static readonly COD_TIP_DOCIDE_C_DIPLO = '16';
    static readonly COD_TIP_DOCIDE_N_INTER_DEPEND = '25';
    static readonly ERROR_CONSULTANDO_DOCIDE = "No se encontr\u00f3 informaci\u00f3n del Documento de Identidad";

    static readonly COD_TIP_CONTRATO_ARRENDAMIENTO = '01';
    static readonly COD_TIP_CONTRATO_SUBARRENDAMIENTO = '02';
    static readonly COD_TIP_CONTRATO_CESION_GRATUITA = '03';

    static readonly CONTRATO_GRABADO_OK = 'El Contrato de Arrendamiento se ha grabado correctamente';
    static readonly ERROR_CONSULTADO_RESUMEN = 'No se pudo obtener informaci\u00f3n del Contrato';
    static readonly DECLARACION_GENERADA = 'Se ha generado la Cuponera Virtual de Pagos Nro. {numCuponera} de la Declaraci\u00f3n Informativa del Contrato Nro. {numDeclaInformativa}.';

    static readonly ERROR_PRINT_CONTRATO = 'Ocurri\u00f3 un error al generar la impresi\u00f3n';
    static readonly ERROR_DESCARGA_CONTRATO = 'Ocurri\u00f3 un error al generar la descarga';
    static readonly ERROR_CONSULTA_CUPONERA = 'Ocurri\u00f3 un error al obtener informaci\u00f3n de la Cuponera';

    static readonly TITULO_MENSAJE_INFO_CUPONERA = 'Sr. Contribuyente';
    static readonly DETALLE_MENSAJE_INFO_CUPONERA = 'El presente documento denominado Cuponera Virtual de Pagos le muestra sus obligaciones tributarias conforme a la declaraci\u00f3n informativa de contratos de arrendamiento que Ud. ha llenado tal como consta en los sistemas de SUNAT. Con este documento Ud. podr\u00e1 cancelar sus obligaciones indicando \u00fanicamente el N\u00famero de Pago de Arrendamiento conforme a los mecanismos que existen para el cumplimiento de sus obligaciones.';
    static readonly TITULO_MENSAJE_OBS_CUPONERA = 'Observaciones';
    static readonly DETALLE_MENSAJE_OBS_CUPONERA = 'La Cuponera Virtual podr\u00e1 ser utilizada para el cumplimiento de sus obligaciones tributarias por el alquiler de bienes.<br>' +
        '(*) El c\u00e1lculo del impuesto a pagar es referencial a raz\u00f3n del 5% sobre el monto de alquiler en la moneda pactada. <br>' +
        '(**) Se consigna la informaci\u00f3n del cronograma de vencimiento que la SUNAT dispone al momento de la emisi\u00f3n de la Cuponera Virtual de Pagos. <br>' +
        '(***) La informaci\u00f3n adicional le muestra si el cupo\u00f3n se encuentra activo o inactivo (respecto a la caducidad del documento) y si se encuentra pagado o no pagado (respecto al estado de la obligaci\u00f3n tributaria).';

    static readonly ERROR_CONSULTADO_CONTRATO = 'No se pudo obtener informaci\u00f3n del Contrato';
    static readonly ERROR_CONTRATO_SIN_CAMBIOS = 'Estimado Contribuyente, no se detectaron cambios en los datos del Contrato';
    static readonly ERROR_MODIFICAR_CONTRATO = 'Ocurri\u00f3 un error al modificar el Contrato, por favor reintente luego';
    static readonly MENSAJE_MODIFICAR_DECLARACION = '¿Est\u00e1 seguro de modificar la Declaraci\u00f3n Informativa de Contrato de Arrendamiento?';
    static readonly MENSAJE_REGISTRAR_DECLARACION = '¿Est\u00e1 seguro de registrar una nueva Declaraci\u00f3n Informativa de Contrato de Arrendamiento?';
    static readonly MENSAJES_NO_DATA = 'No existe informaci\u00f3n para la consulta realizada';
}
