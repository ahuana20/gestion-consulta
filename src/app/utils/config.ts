export const dtOptions = {
    responsive: true,
    dom: 'lrtip',
    pagingType: 'full_numbers',
    aoColumnDefs: [{ bSortable: false, aTargets: [0, 1] }],
    order: [[0, 'desc'], [1, 'none']],
    retrieve: true,
    language: {
        lengthMenu: 'Mostrar _MENU_ registros por página',
        zeroRecords: 'No hay registros disponibles',
        info: 'Mostrando _PAGE_ de _PAGES_',
        infoEmpty: 'No hay registros disponibles',
        infoFiltered: '(filtrando de _MAX_ registros totales)',
        search: 'Buscar:',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: 'Siguiente',
            previous: 'Anterior'
        },
    }
};


export const I18N_VALUES = {
    es: {
       weekdays: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
       months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic'],
   }
};

export const dtOptions2 = {
    responsive: true,
    dom: 'lrtip',
    pagingType: 'full_numbers',
   // aoColumnDefs: [{ bSortable: false, aTargets: [0, 1] }],
    order: [[0, 'asc'], [1, 'none']],
    retrieve: true,
    language: {
        lengthMenu: 'Mostrar _MENU_ registros por página',
        zeroRecords: 'No hay registros disponibles',
        info: 'Mostrando _PAGE_ de _PAGES_',
        infoEmpty: 'No hay registros disponibles',
        infoFiltered: '(filtrando de _MAX_ registros totales)',
        search: 'Buscar:',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: 'Siguiente',
            previous: 'Anterior'
        },
    }
};

