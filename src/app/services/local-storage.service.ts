import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ConstantesUris } from '../utils/constantes-uris';
import { isNotEmpty } from '../utils/utilitarios';
import { switchMap } from 'rxjs/operators';
import { UserData } from '../models/user-data.model';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(private http: HttpClient) { }

  consultarParametria(): Observable<any> {
    return this.http.get<any>(`${ConstantesUris.URI_PARAMETRIA}`);
  }

  async guardarParametria() {
    if (isNotEmpty(this.obtenerLocalStorage('parametria'))) {
      return;
    }
    let result = await this.consultarParametria().pipe(
      switchMap(result => result ? this.guardarLocalParametria(result) : of({}))
    ).toPromise().then(() => { });
  }

  guardarLocalParametria(result) {
    this.guardarLocalStorage('parametria', JSON.stringify(result));
    return of({});
  }

  guardarLocalStorage(key, value) {
    if (isNotEmpty(value)) {
      localStorage.setItem(key, value);
    }
  }

  obtenerLocalStorage(key) {
    return localStorage.getItem(key);
  }

  cargarParametria(idParametro: string) {
    const dataParametria = JSON.parse(localStorage.getItem('parametria'));
    const parametria = dataParametria.data.find(a => a.codParam == idParametro);
    return parametria;
  }

  generarUID(): string {
    let d = new Date().getTime();
    if (window.performance && typeof window.performance.now === 'function') {
      d += performance.now();

    }
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {

      // tslint:disable-next-line:no-bitwise
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);

      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  guardarUID() {
    sessionStorage.setItem('uid', this.generarUID());
  }

  guardarToken() {
    sessionStorage.setItem('SUNAT.token', 'eyJraWQiOiJhcGkuc3VuYXQuZ29iLnBlLmtpZDIwMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIiLCJhdWQiOiJbe1wiYXBpXCI6XCJodHRwczpcL1wvYXBpLWludHJhbmV0LnN1bmF0LnBlcnVcIixcInJlY3Vyc29cIjpbe1wiaWRcIjpcIlwvdjFcL2p1cmlkaWNhXC9leHBlZGllbnRlc1wiLFwiaW5kaWNhZG9yXCI6XCIwXCIsXCJndFwiOlwiMTAwMTAwXCJ9XX1dIiwidXNlcmRhdGEiOnsibnVtUlVDIjoiIiwidGlja2V0IjoiMjE4MTE2MTM1NzEwOSIsIm5yb1JlZ2lzdHJvIjoiNjc1OCIsImFwZU1hdGVybm8iOiJIVUFNQU4iLCJsb2dpbiI6IkVST1NBUyIsIm5vbWJyZUNvbXBsZXRvIjoiRWR3YXJkIFJvc2FzIEFsdmEiLCJub21icmVzIjoiRURXQVJEIiwiY29kRGVwZW5kIjoiMDA3MSIsImNvZFRPcGVDb21lciI6IiIsImNvZENhdGUiOiJTNSIsIm5pdmVsVU8iOjYsImNvZFVPIjoiMVUyMzAzIiwiY29ycmVvIjoicHJ1ZWJhc0BzdW5hdC5nb2IucGUiLCJ1c3VhcmlvU09MIjoiIiwiaWQiOiIiLCJkZXNVTyI6IkRJVklTScOTTiBERSBERVNBUlJPTExPIERFIFNJU1RFTUFTIEFETUlOSVNUUkFUSVZPUyAtIFNVUEVSVklTScOTTiAzIiwiZGVzQ2F0ZSI6IkVTUEVDSUFMSVNUQSAyIiwiYXBlUGF0ZXJubyI6IlJPU0FTIEFMVkEiLCJpZENlbHVsYXIiOm51bGwsIm1hcCI6eyJjb2RVT1ZpcyI6IjFVMjMwMyIsImluZEFkdWFuYSI6IlMiLCJjb2RBbnRlIjoiQTYxNCIsIm5pdmVsX2Z1bmMiOm51bGwsInJvbGVzIjp7IkNPRl9QU1JfQUdFTlRFX0ZJU0NBTElaQUQiOiJDT0ZfUFNSX0FHRU5URV9GSVNDQUxJWkFEIiwiSVFCRl9UTV9TVVBFUlZJU09SIjoiSVFCRl9UTV9TVVBFUlZJU09SIiwiSVFCRl9BQ0ZfQVVESVRPUl9BR0VOVEVfUCI6IklRQkZfQUNGX0FVRElUT1JfQUdFTlRFX1AiLCJPUklFTlRBRE9SIE1JR0UiOiJPUklFTlRBRE9SIE1JR0UiLCJVU1VBUklPX0xJREVSIjoiVVNVQVJJT19MSURFUiIsIklRQkZfSU5GU0FOX1NVUEVSVklTT1JfT0kiOiJJUUJGX0lORlNBTl9TVVBFUlZJU09SX09JIiwiU1VQRVJWSVNPUiBNSUdFIjoiU1VQRVJWSVNPUiBNSUdFIiwiQ0VCQUYtUk9MLVNVUEVSVklTT1IiOiJDRUJBRi1ST0wtU1VQRVJWSVNPUiIsIklRQkZfQUNGX0NPTlNVTFRBUyI6IklRQkZfQUNGX0NPTlNVTFRBUyIsIkFOQUxJU1RBIE1JR0UiOiJBTkFMSVNUQSBNSUdFIiwiQVVESVRPUi1WRVJJRklDQURPUiBNSUdFIjoiQVVESVRPUi1WRVJJRklDQURPUiBNSUdFIn0sImlkTWVudSI6IjIxODExNjEzNTcxMDkiLCJ0aXBPcmlnZW4iOiJJQSIsInRpcFBlcnMiOm51bGx9fSwibmJmIjoxNjI1MDg3NjA3LCJjbGllbnRJZCI6IjU1NzU5MDhhLWQ0OTEtNDg2My1hYmU1LTNlYjZiMzg4YWU5YSIsImlzcyI6Imh0dHBzOlwvXC9hcGktc2VndXJpZGFkLnN1bmF0LmdvYi5wZVwvdjFcL2NsaWVudGVzc3VuYXRcLzU1NzU5MDhhLWQ0OTEtNDg2My1hYmU1LTNlYjZiMzg4YWU5YVwvb2F1dGgyXC90b2tlblwvIiwicHJvZmlsZXMiOlsiU0lSSC1FTVBMRUFETyIsIkRFTlVOQ0lBLVVTVUFSSU8iLCJUUkFOU1AtQVNJR05BQ0lPTiIsIlRSQU5TUC1FVkFMVUFDSU9OIiwiQ1YtQ09OU1VMVEFTIiwiVFJBRElOIC0gQURNSU5JU1RSQURPUiIsIk1PVkNBUi1DT05TVUxUQS1JTkdSRVNPIiwiUEVSRklMLUJGLUNPTlNVTFRBUy1HUkUiLCJFU1BFQ19BRFVBTkVSTyIsIkNPTlNVTF9BVVRPUklaQUlRQkYiLCJPUEVSQURPUi1FVEkiLCJTVVBFUlZJU09SIERTUCIsIlRSSVBVTEFOVEUtRlVOQ0FEVUEtR0VORVIiLCJQRVJGSUwtQ09OU1VMVEFTLUdSRS1NRyIsIlRSQUJBSkFET1ItU0NMIiwiQURNSU5JU1RSQURPUi1TQ0wiLCJTSUVWX1VTVUFSSU9fSU5URVJOTyIsIlNJRVZfUkVHSVNfUkVRIiwiSkVGRSBBUkVBIERFIEFVRElUT1JJQSIsIlZFUklGSUNBRE9SIiwiU0RBX0RFU1RJTkFDSU9OLUFEVUFORVJBIiwiRlZfUkVOVEEtQVlVREEiLCJGVjE2NjctQUNULUlORElDQUQiLCJUUkFOU1AtQURNSU4iLCJJUUJGLVNVUEVSVklTT1ItTk9USSIsIklRQkYtR0VSRU5URS1OT1RJIiwiSVFCRl9JTlNfTUFOVEVOSU1JRU5UTyIsIlNJUkgtUEVSU09OQUwtQU5BTElTVEEiLCJDQVRBTE9HT1MtQURNSU5JU1RSQURPUiIsIkNPTlNfREVWUE5fTUFTSVZPIiwiQkFKX09GSUNJTyIsIlJFR0lTVFJPIFZFUy1TTEUiLCJQSURFLUNPTlMgU1VOQVJQIiwiUElERS1DT05TIFNVTkFSUC1HRVNUSU9OIiwiQ0FQLUNBUy1BTkFMSVNUQSIsIkNPTlNVTFRBUyBHRVNUScOTTiBPU0UiLCJFVkFMVUFET1IgR0VTVEnDk04gT1NFIiwiRU1JU09SIEdFU1RJw5NOIE9TRSIsIlNJRi1HRVNULUNBUlRFUkEtVFJBQkFKTyIsIlNJRi1HRVNULVJPTCIsIlNJRi1HRVNULUNFTlRSTy1DT05UUk9MIiwiU0lGLUdFU1QtUkVDVVJTTyIsIlNJRi1HRVNULUVRVUlQTy1UUkFCQUpPIiwiU0lGLUNPTi1GSVNDQUxJWkFCTEUiLCJTSUZfQ09OX09SSUVOX1BSRVNFTkNJQUwiLCJTSUZfQ09OX1JFUE9SVEVfREVUQUxMRSIsIlNRUy1TVVBFUlZJU09SIiwiU0lGLUdFU1QtSE9SQVJJTyIsIlNJRi1DQUxFLUFURU5DSU9OIiwiU0lGLUdFU1QtQ0FNQklPLUNJVEEiLCJQSURFLUNPTlMgT1JJRU5UQURPUiBDU0MiLCJQSURFIENPTlMtU0VSVklDLlwvQ09OVFJBVCIsIkRFTlVOQ0lBLU9QRVJBLUNBUkdBLU1BU0kiLCJJUUJGLVJFTk9WQUNJT04tU1VQRVJWSVNPIiwiQURVQU5BLUpFRkUtTVVBIiwiU0lHQV9EREpKX0FOQVBMQU5JIiwiVFJBTlNQX0NPTlNVTFRBIiwiSVFCRi1HRVJFTlRFLUNPTlRST0xUUkFOUyIsIkNPRl9QU1JfUFJPR1JBTUFET1JfQ0VOVFIiLCJDT0ZfUFNSX1NVUEVSVklTT1JfUkVDTEFNIiwiU0lFVl9DSUVSUkVfTUFTSVZPX0VYUCIsIkFETUlOIFBFUlNPTkFMSVpBRE8iLCJDT05TVUxUQS1JTlRFLUFEVUFORVJBIiwiQUNFLUZVTkNJT05BLVBVRVNUT0NPTlRSTyIsIlVTVUFSSU9fTUFOVF9TQUxET1MiXSwiZXhwIjoxNjI1MTIzNjM3LCJncmFudFR5cGUiOiJwYXNzd29yZCIsImlhdCI6MTYyNTA4NzYzN30.zyFLAmpvp4vFcVmhK80vqyYSY_rRd8-mHkHE4w5qau7WpDMEJDdX5Ripam0CoW1au1r1oaZctduswZbx4TZXwtoghWdWw7A15yufaXo3I_ICFo9ESMqZI_5S0xkLkgc0gKyLMoGsMdCx01-M3N580b_IbOehmsCv9Tu9TG7XSXIoLJBFscQIeRp_WMkY9chdfhZF_anl4jh0WDfiCvZQfpz_JxBNRQTNJ9-c_DBdrVlAWro6Lf4IyFYvaW2mRf9zFiUn3VKpP-lwMPyyZtN4AElbqiMNglEfLlI_55adUGKd-Nh4BcqwD2bZYftjN2cueWSSa8j5byR_Mqgpi_kQ0Q');
  }

  guardarTipoDeUsuario() {
    sessionStorage.setItem('SUNAT.userdata', JSON.stringify(new UserData()));
  }

  obtenerTipoDeUsuario() {
    let userData: UserData;
    return userData = JSON.parse(sessionStorage.getItem('SUNAT.userdata'));
  }

  borrarSessionStorage() {
    sessionStorage.removeItem('SUNAT.token');
  }
}
