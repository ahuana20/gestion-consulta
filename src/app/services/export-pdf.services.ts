import { Injectable } from '@angular/core';
import jsPDF from 'jspdf';
import { ExportModel } from '../models/export.model';
import { from } from 'rxjs';
import { map, toArray } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ExportPdfService {
  constructor() {}

  async exportPDF(pdfData: ExportPdfModel, print: boolean = false) {
    var headers = this.createHeaders(pdfData.headers, pdfData.keys);
    var data = this.generateData(pdfData.data, pdfData.keys);

    const doc = new jsPDF({ putOnlyUsedFonts: true, orientation: 'landscape' });

    doc.table(1, 1, data, headers, { autoSize: false, headerBackgroundColor: "#007cb7" });

    if (print) {
      doc.autoPrint();
      doc.output('dataurlnewwindow');
    } else {
      doc.save(pdfData.title + '.pdf');
    }
  }

  private generateData(datos: any[], keys: any[]) {
    const response: any[] = [];

    datos.forEach((item: any) => {
      let newValue: any = {};
      for (let key of keys) {
        if (item[key]) {
          if (String(item[key])) {
            newValue[key] = String(item[key]);
          } else {
            newValue[key] = '';
          }
        } else {
          newValue[key] = '';
        }
      }

      response.push(newValue);
    });

    return response;
  }

  private createHeaders(headers: any[], keys: any[]) {
    var result = [];
    for (var i = 0; i < keys.length; i += 1) {
      result.push({
        id: keys[i],
        name: keys[i],
        prompt: headers[i],
        align: 'center',
        width: 65,
        padding: 0,
      });
    }
    return result;
  }
}

export class ExportPdfModel {
  title: string;
  data: any[];
  headers: any[];
  keys: any[];
}
