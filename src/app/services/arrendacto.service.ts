import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantesUris } from '../utils/constantes-uris';

@Injectable({
  providedIn: 'root',
})
export class ArrendactoService {
  constructor(private utilService: UtilService) {}

  validarAfectacionRUC(numRuc: string): Observable<any> {
    let url = `${ConstantesUris.URI_PERMITIR_DECLARACION}`.replace(
      '{numRuc}',
      numRuc
    );
    return this.utilService.callGET(url);
  }

  async validarAfectacionRUCPromise(numRuc: string): Promise<any> {
    return await new Promise((resolve, reject) => {
      this.validarAfectacionRUC(numRuc)
        .toPromise()
        .then((res: any) => {
          resolve(res);
        })
        .catch((error: any) => {
          resolve({ success: false, msg: 'El recurso solicitado no existe' });
        });
    });
  }
}
