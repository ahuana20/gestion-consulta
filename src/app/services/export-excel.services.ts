import { Injectable } from '@angular/core';
import * as fileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { ExportModel } from '../models/export.model';

const EXCEL_TYPE =
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root',
})
export class ExportExcelService {
  constructor() {}

  exportExcel(exportModel: ExportModel) {
    var data = this.generateData(exportModel.data, exportModel.headers, exportModel.keys);
    this.exportAsExcelFile (data, exportModel.title);
  }

  // tslint:disable-next-line:typedef
  private generateData(datos: any[], headers: any [], keys: any[]) {
    const response: any[] = [];

    datos.forEach((item: any) => {
      let newValue: any = {};

      for (let index = 0; index < headers.length; index++) {
        if (item[keys[index]]) {
          if (item [keys[index]]) {
            newValue[headers[index]] = item[keys[index]];
          }
        } else {
          newValue[headers[index]] = '';
        }
      }

      response.push(newValue);
    });

    return response;
  }

  private exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ['data'],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'array',
    });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    fileSaver.saveAs(
      data,
      fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
    );
  }
}
