import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable} from 'rxjs';
import { isNotEmpty, trim } from '../utils/utilitarios';
import { Constantes } from '../utils/constantes';
import { ModalConfirmacionComponent } from '../components/utils/modal-confirmacion/modal-confirmacion.component';
@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private http: HttpClient, private modalService: NgbModal) { }

  callPOST(url: string, bean?: any): Observable<Response> {
    const parametro: string = isNotEmpty(bean) ? JSON.stringify(bean) : '{}';
    let myHeader = new HttpHeaders();
    myHeader = myHeader.set('Content-Type', 'application/json');
    return this.http.post<Response>(url, parametro, { headers: myHeader });
  }

  callPUT(url: string, bean?: any): Observable<Response> {
    const parametro: string = isNotEmpty(bean) ? JSON.stringify(bean) : '{}';
    let myHeader = new HttpHeaders();
    myHeader = myHeader.set('Content-Type', 'application/json');
    return this.http.put<Response>(url, parametro, { headers: myHeader });
  }

  callDELETE(url: string, bean?: any): Observable<Response> {
    let myHeader = new HttpHeaders();
    myHeader = myHeader.set('Content-Type', 'application/json');
    return this.http.delete<Response>(url, { headers: myHeader });
  }

  callGET(url: string): Observable<any> {
    return this.http.get (url);
  }

  private mensajeConfirmacion(titulo, mensaje, tipoModal, claseModal, fnSuccess?: () => void, fnDanger?: () => void) {
    const modal = {
      titulo : titulo,
      mensaje : mensaje
    };

    const modalRef = this.modalService.open(ModalConfirmacionComponent, { backdrop: 'static', keyboard: false });
    modalRef.componentInstance.modal = modal;
    modalRef.componentInstance.tipoModal = tipoModal;
    modalRef.componentInstance.classModal = claseModal;
    modalRef.componentInstance.respuesta.subscribe(data => {
      if (trim(data) === 'SI') {
        if (typeof fnSuccess === 'function') {
          fnSuccess();
        }
      } else if (trim(data) === 'NO') {
        if (typeof fnDanger === 'function') {
          fnDanger();
        }
      }
    });
  }

  modalMensaje(titulo, mensaje, claseModal) {
    this.mensajeConfirmacion(titulo, mensaje, Constantes.MODAL_OK, claseModal);
  }

  modalMensajePersonalizado(titulo, mensaje, vistaBotones, claseModal) {
    this.mensajeConfirmacion(titulo, mensaje, vistaBotones, claseModal);
  }

  modalMensajeError(mensaje) {
    this.modalMensaje(
      Constantes.MODAL_ERROR_TITULO,
      mensaje,
      Constantes.MODAL_DANGER
    )
  }

  modalMensajeInfo(mensaje) {
    this.modalMensaje(
      Constantes.MODAL_INFO_TITULO,
      mensaje,
      Constantes.MODAL_PRIMARY
    )
  }

  modalMensajeSuccess(mensaje) {
    this.modalMensaje(
      Constantes.MODAL_SUCCESS_TITULO,
      mensaje,
      Constantes.MODAL_SUCCESS
    );
  }

  modalConfirmacion(titulo, mensaje, tipoModal, claseModal, fnSuccess?: () => void, fnDanger?: () => void) {
    this.mensajeConfirmacion(titulo, mensaje, tipoModal, claseModal, fnSuccess, fnDanger);
  }
}
