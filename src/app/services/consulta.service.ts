import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { HttpClient } from '@angular/common/http';
import { ConstantesUris } from '../utils/constantes-uris';

@Injectable({
  providedIn: 'root',
})
export class ConsultaService {
  constructor(private utilService: UtilService) {}
}
