import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { ConstantesUris } from '../utils/constantes-uris';
import { requestFilter } from '../utils/util';
import { ResponseRequest } from '../models/response-request';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Contrato } from '../models/contrato.model';

@Injectable({
  providedIn: 'root',
})
export class ArrendaConsultaService {
  constructor(private utilService: UtilService) {}

  contratoConsulta(numRuc: string, request: any): Observable<Contrato[]> {
    const params = requestFilter(request);

    let url = `${ConstantesUris.URI_CONTRATO_CONSULTA}`
      .replace('{numRuc}', numRuc)
      .replace('{params}', params);
    return this.utilService.callGET(url).pipe(
      map((response: ResponseRequest) => {
        if (response.success) {
          return response.data;
        } else {
          throw new Error(response.msg).message;
        }
      })
    );
  }

  obtenerReporteDeclaracion(numDecla: string): Observable<any> {
    let url = `${ConstantesUris.URI_REPORTE_CONTRATO}`.replace(
      '{numDeclaInfor}',
      numDecla
    );
    return this.utilService.callGET(url);
  }

  
}
