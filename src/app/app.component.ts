import { Component} from '@angular/core';
import { LocalStorageService } from './services/local-storage.service';
import { ThemeSpinnerService } from './theme/services/theme-spinner.service';
import { environment } from 'src/environments/environment';
// import { ConsultaContratosService } from './services/consulta-contratos.service';
import { UtilService } from './services/util.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private _spinner: ThemeSpinnerService,
              private localStorageService: LocalStorageService,
              private utilService: UtilService) {
  }

  ngOnInit() {
    this._spinner.hide();
    if (!environment.production) {
      this.localStorageService.guardarTipoDeUsuario();
      this.localStorageService.guardarToken();
    }
  }
}
