export const environment = {
  production: true,
  uri_base: 'https://api-intranet.sunat.peru/v1/recaudacion/tributaria/administracion/arrendacto/',
  url_consulta: 'https://api-intranet.sunat.peru/v1/recaudacion/tributaria/administracion/arrendaconsulta/',
};
